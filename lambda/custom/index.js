"use strict";
const Alexa = require('ask-sdk-core');
const Axios = require('axios');
const moment = require('moment');

const SKILL_NAME = "Carbon Footprint";

const BASE_URL = "https://carbonhub.org/v1";
const API_KEY = process.env.CARBON_HUB_API_KEY;

const endpoints = {
    EMISSIONS_ENDPOINT: BASE_URL + "/emissions"
};

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        let speechText = 'Welcome to Carbon Assistant. You can say, how much carbon dioxide is emitted when an air conditioner is used?';
        let repromptText = 'You can say, how much carbon dioxide is emitted when an air conditioner is used?';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(repromptText)
            .getResponse();
    },
};

const InProgressApplianceEmissionIntentHandler = {
    canHandle(handlerInput) {
        const request = handlerInput.requestEnvelope.request;
        return request.type === 'IntentRequest' &&
            request.intent.name === 'applianceEmissionIntent' &&
            request.dialogState !== 'COMPLETED';
    },
    handle(handlerInput) {
        const currentIntent = handlerInput.requestEnvelope.request.intent;
        return handlerInput.responseBuilder
            .addDelegateDirective(currentIntent)
            .getResponse();
    },
};

const ApplianceEmissionIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'applianceEmissionIntent';
    },
    async handle(handlerInput) {
        const responseBuilder = handlerInput.responseBuilder;
        const attributesManager = handlerInput.attributesManager;
        let sessionAttributes = attributesManager.getSessionAttributes() || {};

        let appliance = slotValue(handlerInput.requestEnvelope.request.intent.slots.appliance);
        let country = slotValue(handlerInput.requestEnvelope.request.intent.slots.country);
        let time = slotValue(handlerInput.requestEnvelope.request.intent.slots.time);
        let quantity = slotValue(handlerInput.requestEnvelope.request.intent.slots.quantity);
        let emission = slotValue(handlerInput.requestEnvelope.request.intent.slots.emission);

        sessionAttributes.item = appliance;

        if (country == "") {
            sessionAttributes.region = 'Default';
        }
        else {
            sessionAttributes.region = country;
        }

        if (time == "") {
            sessionAttributes.multiply = 1;
        }
        else {
            sessionAttributes.multiply = moment.duration(time, moment.ISO_8601).asHours();
        }

        if (quantity == "") {
            sessionAttributes.quantity = 1;
        }
        else {
            sessionAttributes.quantity = quantity;
        }

        if (emission == "") {
            sessionAttributes.emissionType = 'all';
        }
        else {
            sessionAttributes.emissionType = emission;
        }

        let speechText = await new Promise((resolve, reject) => {
            resolve(getApplianceEmissions(sessionAttributes));
        });

        return responseBuilder
            .speak(speechText)
            .getResponse();
    },
};

async function getApplianceEmissions(sessionAttributes) {
    let options = {
        headers: {
            'cache-control': 'no-cache',
            'access-key': API_KEY,
            'Content-Type': 'application/json'
        },
        body: {
            item: sessionAttributes.item,
            region: sessionAttributes.region,
            quantity: sessionAttributes.quantity,
            multiply: sessionAttributes.multiply
        },
        json: true
    };

    let speechText = "";

    await Axios.post(endpoints.EMISSIONS_ENDPOINT,
        options.body, {
            headers: options.headers
        })
        .then(res => res.data)
        .then(res => {

            let emissionValue;
            let emissionUnit;

            if (sessionAttributes.emissionType != 'all') {
                emissionValue = res.emissions[sessionAttributes.emissionType];
                emissionUnit = res.unit;
                speechText = `${sessionAttributes.item} produces ${emissionValue.toFixed(3) > 0.000 ? emissionValue.toFixed(3) : emissionValue.toFixed(6)} ${emissionUnit} of ${sessionAttributes.emissionType}.`;
            }
            else {
                emissionUnit = res.unit;
                speechText = `${sessionAttributes.item} produces ${res.emissions['CO2'].toFixed(3) > 0.000 ? res.emissions['CO2'].toFixed(3) : res.emissions['CO2'].toFixed(6)} ${emissionUnit} of Carbon Dioxide, ${res.emissions['CH4'].toFixed(3) > 0.000 ? res.emissions['CH4'].toFixed(3) : res.emissions['CH4'].toFixed(6)} ${emissionUnit} of Methane and ${res.emissions['N2O'].toFixed(3) > 0.000 ? res.emissions['N2O'].toFixed(3) : res.emissions['N2O'].toFixed(6)} ${emissionUnit} of Nitrous oxide.`;
            }
        })
        .catch(err => {
            console.log(err.message);
            speechText = "An unknown error occured. Please contact our support. Error Code is " + err.response.status;
        });

    return speechText;
}

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText = 'You can ask me how much carbon dioxide is emitted when an air conditioner is used?';

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    },
};

const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = 'Goodbye!';

        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    },
};

const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        console.log(`Session ended with reason: ${handlerInput.requestEnvelope.request.reason}`);

        return handlerInput.responseBuilder.getResponse();
    },
};

const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.log(`Error handled: ${error.message}`);

        return handlerInput.responseBuilder
            .speak('Sorry, I can\'t understand the command. Please say again.')
            .reprompt('Sorry, I can\'t understand the command. Please say again.')
            .getResponse();
    },
};

function slotValue(slot, useId) {
    if (slot.value == undefined) {
        return "";
    }
    let value = slot.value;
    let resolution = (slot.resolutions && slot.resolutions.resolutionsPerAuthority && slot.resolutions.resolutionsPerAuthority.length > 0) ? slot.resolutions.resolutionsPerAuthority[0] : null;
    if (resolution && resolution.status.code == 'ER_SUCCESS_MATCH') {
        let resolutionValue = resolution.values[0].value;
        value = resolutionValue.id && useId ? resolutionValue.id : resolutionValue.name;
    }
    return value;
}

const skillBuilder = Alexa.SkillBuilders.custom();

exports.handler = skillBuilder
    .addRequestHandlers(
        LaunchRequestHandler,
        InProgressApplianceEmissionIntentHandler,
        ApplianceEmissionIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler
    )
    .addErrorHandlers(ErrorHandler)
    .lambda();
